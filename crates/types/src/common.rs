// SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
//
// SPDX-License-Identifier: EUPL-1.2

//! This module contains types that are used in different areas of the OpenTalk API,
//! such as the Web API and signaling.

pub mod shared_folder;
pub mod tariff;
